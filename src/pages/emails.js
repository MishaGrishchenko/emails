import Box from '@mui/material/Box';
import CurrentUser from '../components/currentUser';
import SendEmail from '../components/sendEmail';
import EmailsList from '../components/emailsList';

function EmailsPage() {
  return (
    <Box component="section" className="emails_page">
     <CurrentUser/>
     <SendEmail/>
     <EmailsList/>
    </Box>
  );
}

export default EmailsPage;
