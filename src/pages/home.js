import React, { useState } from 'react';
import Box from '@mui/material/Box';
import Auth from '../components/auth';
import Registration from '../components/registration';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';

function HomePage() {
  const [value, setValue] = useState('1');

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <Box component="section" className='home_page'>
      <Box>
        <TabContext value={value}>
          <Box>
            <TabList onChange={handleChange} className='tabList'>
              <Tab label="Sing in" value="1" />
              <Tab label="Sing up" value="2" />
            </TabList>
          </Box>
          <TabPanel value="1"><Auth/></TabPanel>
          <TabPanel value="2"><Registration/></TabPanel>
        </TabContext>
      </Box>
    </Box>
  )
}

export default HomePage;