import React, { useState, useEffect } from 'react';
import Paper from '@mui/material/Paper';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { addEmails } from '../../store/emailsSlice';

function EmailsList() {
  const emails = useSelector((state) => state.emails.emails);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState('');
  let username = useSelector((state) => state.user.username);
  let password = useSelector((state) => state.user.password);
  let userId = useSelector((state) => state.user.id);
  const dispatch = useDispatch();
  const [currentPage, setCurrentPage] = useState(1);
  const [emailsPerPage] = useState(5);

  useEffect(() => {
    const fetchEmails = async () => {
      try {
        if (!username || !password) {
          username = localStorage.getItem('login');
          password = localStorage.getItem('password');
        }
        const credentials = btoa(`${username}:${password}`);
        const response = await axios.get('http://68.183.74.14:4005/api/emails/', {
          headers: {
            'Authorization': `Basic ${credentials}`,
            'Content-Type': 'application/json'
          },
          mode: 'cors'
        });

        dispatch(addEmails(response.data.results));
        setLoading(false);
      } catch (error) {
        setError('Failed to fetch emails. Please try again later.');
        setLoading(false);
      }
    };

    fetchEmails();
  }, []);

  const indexOfLastEmail = currentPage * emailsPerPage;
  const indexOfFirstEmail = indexOfLastEmail - emailsPerPage;
  const currentEmails = emails.slice(indexOfFirstEmail, indexOfLastEmail);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  return (
    <div className='list_emails'>
      <Paper sx={{ padding: '15px' }}>
        <h2>Emails List</h2>
        {loading ? (
          <p>Loading...</p>
        ) : error ? (
          <p>{error}</p>
        ) : (
          <div>
            <div className='count'>Count: {emails.length}</div>
            <ul>
              {currentEmails.map((email, index) => {
                if (email.sender === userId) {
                  return (
                    <li key={index}>
                      <div><strong>Sender:</strong> {username}</div>
                      <div><strong>Subject:</strong> {email.subject}</div>
                      <div><strong>Recipient:</strong> {email.recipient}</div>
                    </li>
                  )
                }
              })}
            </ul>
            <ul className="pagination">
              {Array.from({ length: Math.ceil(emails.length / emailsPerPage) }).map((_, index) => (
                <li key={index} className={currentPage === index + 1 ? 'active' : ''}>
                  <button onClick={() => paginate(index + 1)}>{index + 1}</button>
                </li>
              ))}
            </ul>
          </div>
        )}
      </Paper>
    </div>
  );
}

export default EmailsList;
