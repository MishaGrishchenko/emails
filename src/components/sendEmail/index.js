import React, { useState } from 'react';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios';
import { addEmail } from '../../store/emailsSlice';
import {Editor, EditorState} from 'draft-js';
import 'draft-js/dist/Draft.css';

function SendEmail() {
  const dispatch = useDispatch();
  const username = useSelector((state) => state.user.username);
  const password = useSelector((state) => state.user.password);
  const userId = useSelector((state) => state.user.id);
  const [showMail, setShowMail] = useState(false);
  const [recipient, setRecipient] = useState('');
  const [subject, setSubject] = useState('');
  const [body, setBody] = useState('');
  const [error, setError] = useState('');

  const handleSendEmail = async () => {
    const senderId = parseInt(userId);

    const emailData = {
      sender: senderId,
      recipient,
      subject,
      message: body
    };

    try {
      const credentials = btoa(`${username}:${password}`);
      const response = await axios.post('http://68.183.74.14:4005/api/emails/', emailData, {
        headers: {
          'Authorization': `Basic ${credentials}`,
          'Content-Type': 'application/json'
        },
        mode: 'cors'
      });

      console.log('Email sent successfully:', response.data);
      dispatch(addEmail(response.data));
      // Optionally, you can provide feedback to the user that the email has been sent
      alert('Email sent successfully!');
      setRecipient('');
      setSubject('');
      setBody('');
    } catch (error) {
      console.error('Error sending email:', error);
      // Display user-friendly error message
      setError('Failed to send email. Please try again later.');
    }
  };


  return (
    <div className='send_email'>
      <Paper sx={{ padding: '15px' }}>
        {!showMail && <Button className='open_from_send_email' onClick={() => setShowMail(!showMail)} variant="contained">Send Email</Button>}
        {showMail && <div className='from_send_email'>
          <div className='usename'>Sender: {username}</div>
          <div>
            <TextField
              label="Recipient"
              variant="standard"
              value={recipient}
              onChange={(e) => setRecipient(e.target.value)}
            />
          </div>
          <div>
            <TextField
              label="Subject"
              variant="standard"
              value={subject}
              onChange={(e) => setSubject(e.target.value)}
            />
          </div>
          <div>
            <TextField
               value={body}
              variant="standard"
              onChange={(e) => setBody(e.target.value)}
              placeholder="Body"
              style={{ width: '100%' }}
            />
          </div>
          {error && <div style={{ color: 'red', marginBottom: '10px' }}>{error}</div>}
          <Button
            className='btn-send_email'
            variant="contained"
            color="success"
            onClick={handleSendEmail}
          >
            Send Email
          </Button>
        </div>}
      </Paper>
    </div>
  );
}

export default SendEmail;
