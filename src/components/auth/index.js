import React, { useState } from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Input from '@mui/material/Input';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { addEmails } from '../../store/emailsSlice';
import { addUsername, addPass } from '../../store/userSlice';

export default function Auth() {
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [error, setError] = useState('');
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleLoginChange = (event) => {
    setLogin(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = async () => {
    const credentials = btoa(`${login}:${password}`);
    try {
      const response = await axios.get('http://68.183.74.14:4005/api/emails/', {
        headers: {
          'Authorization': `Basic ${credentials}`,
          'Content-Type': 'application/json'
        },
        mode: 'cors'
      });

      console.log('Success:', response.data);
      dispatch(addEmails(response.data.results));
      dispatch(addUsername(login));
      dispatch(addPass(password));

      localStorage.setItem('login', login);
      localStorage.setItem('password', password);

      navigate('/emails');
      setError('');
    } catch (error) {
      console.error('Error:', error);
      setError('Failed to fetch emails. Please check your credentials and try again.');
    }
  };

  return (
    <div className='form'>
      <h2>Authorization</h2>
      <TextField
        className='username'
        id="standard-basic"
        label="Login"
        variant="standard"
        value={login}
        onChange={handleLoginChange}
      />
      <FormControl className='pass' sx={{ m: 1, width: '25ch' }} variant="standard">
        <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
        <Input
          id="standard-adornment-password"
          type={showPassword ? 'text' : 'password'}
          value={password}
          onChange={handlePasswordChange}
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
              >
                {showPassword ? <VisibilityOff /> : <Visibility />}
              </IconButton>
            </InputAdornment>
          }
        />
      </FormControl>
      {error && <p style={{ color: 'red' }}>{error}</p>}
      <Button
        className='btn_auth'
        variant="contained"
        onClick={handleSubmit}
      >
        Auth
      </Button>
    </div>
  );
}
