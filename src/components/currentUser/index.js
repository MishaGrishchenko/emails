import React, { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import axios from 'axios';
import { useSelector } from 'react-redux';
import Button from '@mui/material/Button';
import { addEmails } from '../../store/emailsSlice';
import { addUsername, addEmail, addPass, addUserId } from '../../store/userSlice';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

function CurrentUser() {
  const [login, setLogin] = useState(null);
  const [email, setEmail] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState('');
  const usernameEnv = useSelector((state) => state.user.username);
  const passwordEnv = useSelector((state) => state.user.password);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    const fetchCurrentUser = async () => {
      let username = usernameEnv;
      let password = passwordEnv;

      if (!username || !password) {
        username = localStorage.getItem('login');
        password = localStorage.getItem('password');
        if (!username || !password) {
          dispatch(addUsername(username));
          dispatch(addPass(password));
        }
      }

      if (!username || !password) {
        setError('No credentials found. Please log in.');
        setLoading(false);
        return;
      }
      try {
        const response = await axios.get('http://68.183.74.14:4005/api/users/current/', {
          headers: {
            'Authorization': `Basic ${btoa(`${username}:${password}`)}`,
            'Content-Type': 'application/json'
          },
          mode: 'cors'
        });
        console.log(response.data)

        const { id: idRes, username: usernameRes, email: emailRes } = response.data;

        setLogin(usernameRes);
        setEmail(emailRes);

        dispatch(addUsername(usernameRes));
        dispatch(addEmail(emailRes));
        dispatch(addUserId(idRes));

        dispatch(addPass(password));

        localStorage.setItem('userId', idRes);

        setLoading(false);
      } catch (error) {
        console.error('Error fetching user:', error);
        setError('Failed to fetch current user. Please try again later.');
        setLoading(false);
      }
    };

    fetchCurrentUser();
  }, []);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>{error}</div>;
  }

  const handleLogout = () => {
    dispatch(addEmails([]));
    dispatch(addUsername(''));
    dispatch(addPass(''));
    localStorage.removeItem('login');
    localStorage.removeItem('password');
    localStorage.removeItem('userId');
    navigate('/');
  }

  return (
    <Box component="section" className="currentUser">
       <AppBar position="static">
        <Toolbar>
          {login && <div className='user_info'>
            <div>
              <div className='user_info_username'>Username: {login}</div>
              <div className='user_info_email'>Email: {email}</div>
            </div>
            <div className='btn_logout'><Button variant="inherit" onClick={handleLogout}>Logout</Button></div>
          </div>}
        </Toolbar>
      </AppBar>

    </Box>
  );
}

export default CurrentUser;
