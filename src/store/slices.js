// slices.js
import { combineReducers } from 'redux';
import emailsReducer from './emailsSlice';
import userReducer from './userSlice';

const rootReducer = combineReducers({
  emails: emailsReducer,
  user: userReducer,
});

export default rootReducer;
