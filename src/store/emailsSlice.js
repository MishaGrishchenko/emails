// slice.js
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  emails: [],
};

const emailsSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    addEmails: (state, action) => {
      const mails = action.payload;
      state.emails = [...mails]
    },
    addEmail: (state, action) => {
      const mail = action.payload;
      state.emails = [...state.emails, mail]
    },
  },
});

export const { addEmails, addEmail } = emailsSlice.actions;

export default emailsSlice.reducer;
