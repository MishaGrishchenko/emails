// slice.js
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  username: '',
  email: '',
  password: '',
  id: ''
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    addUsername: (state, action) => {
      state.username = action.payload;
    },
    addEmail: (state, action) => {
      state.email = action.payload;
    },
    addPass: (state, action) => {
      state.password = action.payload;
    },
    addUserId: (state, action) => {
      state.id = action.payload;
    },
  },
});

export const { addUsername, addEmail, addPass, addUserId } = userSlice.actions;

export default userSlice.reducer;
