import './App.css';
import HomePage from './pages/home'
import EmailsPage from './pages/emails'

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<HomePage/>} />
          <Route path="/emails" element={<EmailsPage/>} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
